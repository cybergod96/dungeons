#pragma once
#include <SFML/Graphics.hpp>

#define DEFAULT_BAR_HEIGHT 10

class HealthBar: public sf::Drawable
{
private:
	sf::RectangleShape shape;
	int cur_value, max_length, max_value;
	void Update();
public:
	HealthBar(sf::Vector2f pos, int value, int maxl, int maxv, sf::Color color);
	HealthBar() {};
	int GetValue() {return cur_value;}
	void Add(int x) {cur_value += x; Update();}
	void Subtract(int x) {cur_value -= x; Update();}
	void SetCurValue(int x) {cur_value = x; Update();}
	void SetMaxValue(int x) {max_value = x;}
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	~HealthBar(void);
};

