#pragma once
#include <SFML/Graphics.hpp>
#include <direct.h>
#include <fstream>
#include "utils.h"
#include "Map.h"
#include "settings.h"
#include "DeveloperModeState.h"
#include "StateMachine.h"
#include "Logger.h"
#include "AssetManager.h"

sf::RenderWindow window;
sf::Event event;
sf::Font font;
StateMachine *state_machine;