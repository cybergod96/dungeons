#pragma once
#include <string>
#include <sstream>
#include <fstream>
#include <vector>

#define VFS_VERSION 2
#define ASSET_NAME_LENGTH 32

using namespace std;

namespace Framework
{
	enum VirtualFSExceptionType { VFSE_FILE_ERROR, VFSE_VERSION_ERROR, VFSE_UNKNOWN };
#pragma pack(push,1)
	struct VFS_HEADER
	{
		int version, flags, files_count;
	};
#pragma pack(pop)

#pragma pack(push,1)
	struct VFS_FILEINFO
	{
		string name;
		streampos offset, size;

		VFS_FILEINFO(string n, streampos o = 0, streampos s = 0)
		{
			name = n;
			offset = o;
			size = s;
		}
	};
#pragma pack(pop)

	struct VFS_ExtractedFile
	{
		char *data;
		streampos size;

		bool good() { return (data != NULL && size > 0); }
	};

	class VirtualFS
	{
	private:
		vector<VFS_FILEINFO>files_table;
		fstream file;
	public:
		explicit VirtualFS(string file_name);
		~VirtualFS();
		VFS_ExtractedFile GetFile(string name);
		//bool ExtractFile(string file_name, string path);
		//bool ExtractAllFiles(string path);
	};
}

/*
File struct:
-VFS_HEADER = 12 (version, flags, files count)
-file name = 32
-file size = 24

(...)
-file content
(...)

*/

