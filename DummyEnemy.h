#pragma once
#include "Entity.h"
#include "myrandom.h"
#include "AssetManager.h"
class DummyEnemy: public Framework::Entity
{
private:
	//sf::RectangleShape shape;
public:
	virtual Framework::ObjectType GetObjectType() { return Framework::ObjectType::Object_Enemy; }
	DummyEnemy(Coord pos);
	~DummyEnemy();
	void SetPosition(Coord pos);
	void onUpdate();
	void onDeinit();
	void onInit();
	void onInput(const sf::Event &e);
	void onDraw(sf::RenderTarget &target);
	void onCollision(Object *other);
	void onInput(const sf::Event::KeyEvent &key);
	void onDestroy();
	void Turn();
};

