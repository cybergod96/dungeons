#include "DeveloperModeState.h"



DeveloperModeState::DeveloperModeState()
{
	debug_mode = false;
	window_focus = true;
	player = new Player();
	player->LoadResources();
	dungeon = new Dungeon(player);
	dungeon->LoadTileset();
}


DeveloperModeState::~DeveloperModeState()
{
	delete player;
	delete dungeon;
}

void DeveloperModeState::OnEnter(int previousStateId)
{
	return;
}

void DeveloperModeState::OnExit(int nextStateId)
{
	return;
}

void DeveloperModeState::OnSuspend(int pushedStateId)
{
	return;
}

void DeveloperModeState::OnResume(int poppedStateId)
{
	return;
}

void DeveloperModeState::OnDraw(sf::RenderTarget & target, bool suspended)
{
	dungeon->DrawDungeon(target, debug_mode);
}

void DeveloperModeState::OnEvent(sf::Event & event, bool suspended)
{
	if(!suspended)
	{
		if (event.type == sf::Event::KeyPressed)
		{
			if (debug_mode && event.key.code == sf::Keyboard::PageUp) dungeon->Ascend();
			if (debug_mode && event.key.code == sf::Keyboard::PageDown) dungeon->Descend();
			if (event.key.code == sf::Keyboard::F1) debug_mode = !debug_mode;
		}
		else if (event.type == sf::Event::GainedFocus) window_focus = true;
		else if (event.type == sf::Event::LostFocus) window_focus = false;
		dungeon->HandleInput(event);
	}
}

void DeveloperModeState::OnUpdate(bool suspended)
{
	dungeon->Update();
}
