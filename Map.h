#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <memory>
#include "utils.h"
#include "Coord.h"
#include "Object.h"
#include "Player.h"
#include "DummyEnemy.h"
#include "settings.h"
#include "myrandom.h"
#include "AssetManager.h"

#define cur_level levels[depth - 1]

#define MAP_WIDTH 128
#define MAP_HEIGHT 128

#define MIN_ROOM_SIZE 3
#define MAX_ROOM_SIZE 12

#define MIN_CORRIDOR_LENGTH 6
#define MAX_CORRIDOR_LENGTH 12

#define ROOM_CHANCE 70
#define MAX_FEATURES 75

#define MAX_SEGMENTS 100
#define MAX_ENEMIES 10
#define MAX_CHESTS 8

//#define TEST

enum TILE_TYPE { FLOOR, WALL, DOOR, STAIRS_UP, STAIRS_DOWN, EMPTY };
enum DIRECTION {UP, DOWN, LEFT, RIGHT};
enum SEGMENT_TYPE {ROOM, CORRIDOR};

//wsp�rz�dne kafelk�w obiekt�w mapy (w kolejno�ci definicji powy�szego enuma)
const sf::IntRect tile_rects[5] = { sf::IntRect(0,0,32,32),
									sf::IntRect(32,0,32,32),
									sf::IntRect(64,0,32,32),
									sf::IntRect(96,0,32,32),
									sf::IntRect(128,0,32,32)};

struct Segment
{
	SEGMENT_TYPE type;
	Coord pos;
	int width, height; //wymiary ca�ego pokoju (ze �cianmi)
	bool north, south, west, east;

	Segment(SEGMENT_TYPE ntype, Coord npos, int surface_width, int surface_height)
	{
		type = ntype;
		pos = npos;
		width = surface_width+2;
		height = surface_height+2;
		north = south = west = east = true; //mo�na doda�
	}

	Segment()
	{
		type = ROOM;
		north = south = west = east = true;
	}

	inline int GetSurfaceWidth() { return width - 2; }
	inline int GetSurfacelHeight() { return height - 2; }
	inline void Info()
	{
		std::cout << "X: " << pos.x << "\nY: " << pos.y << "\nW: " << width << "\nH: " << height << "\n\n";
	}

	inline Coord RandomPosInside()
	{
		return Coord{randomInt(pos.x + 1, pos.x + (width - 2)),randomInt(pos.y + 1, pos.y + (height - 2))};
	}
};

struct Tile
{
	TILE_TYPE type;
	bool visited;

	Tile()
	{
		type = EMPTY;
		visited = false;
	}
};

struct Map
{	
	Tile tiles[MAP_WIDTH][MAP_HEIGHT];
	std::vector<Framework::Object *>objects;
	//std::vector<std::shared_ptr<Framework::Object>>objects;
	Coord start_pos, stairs_down;
	std::vector<Segment> rooms;
	Map();
	~Map();
	bool CreateSegment(DIRECTION dir, Segment &parent, Coord *door_pos, Segment *destiny);
	void Generate(int depth);
	//bool HasUnvisitedNb(Coord c);
	bool CheckSegment(const Segment &s, const DIRECTION &dir);
	void PlaceSegment(const Segment &s);
	void SetTile(Coord pos, TILE_TYPE t, bool v = true) 
	{ 
		tiles[pos.x][pos.y].type = t; 
		tiles[pos.x][pos.y].visited = v; 
	}
	void SetTile(int x, int y, TILE_TYPE t, bool v = true) 
	{ 
		tiles[x][y].type = t; 
		tiles[x][y].visited = v;
	}
	TILE_TYPE GetTile(int x, int y) { return tiles[x][y].type; }
	void Objects_Update();
	void Objects_HandleInput(const sf::Event &e);
	void Objects_Draw(sf::RenderTarget &target);
	void InsertObject(Framework::Object *o) { objects.push_back(o); o->onInit(); }
	Coord GetStartPos() { return start_pos; }
	Coord GetStairsDownPos() { return stairs_down; }
};

class Dungeon
{
private:
	std::vector<Map *> levels;
	int depth;
	sf::Sprite tileset;
	//sf::Texture tileset_texture;
	//sf::CircleShape tileselection, visitedselection;
	sf::View camera;
	Player *player;
	void LookAtPlayer();
	//void SetMovement(const Coord &pos);
	void SetMovement(Framework::Entity *ent);
	void EnemiesTurn();
	void SetEntitiesMovement();
public:
	void Descend();
	void Ascend();
	void DrawDungeon(sf::RenderTarget &target, bool debug);
	int GetDepth() { return depth; }
	void LoadTileset();
	Dungeon(Player *player);
	~Dungeon();
	inline Coord GetCurMapStartPoint() { return levels[depth - 1]->start_pos; }
	inline Coord GetCurMapStairsDown() { return levels[depth - 1]->stairs_down; }
	void HandleInput(const sf::Event &e);
	void Update();
	
};

