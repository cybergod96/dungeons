#include "AssetManager.h"

AssetManager::AssetManager()
{
}


AssetManager::~AssetManager()
{
	textures.clear();
}

sf::Texture &AssetManager::GetTexture(std::string name)
{
	if (textures.find(name) == textures.end())
	{
		g_logger.Log("[WARNING] Tried to get non-existent texture '%s'.", name.c_str());
		return sf::Texture();
	}
	return *textures[name];
}

bool AssetManager::LoadAssets(std::string path)
{
	//load textures
	sf::Texture tex;
	for (auto &p : std::experimental::filesystem::directory_iterator(path + "/gfx"))
	{		
		/*if(tex.loadFromFile(p.path().generic_string()))
			textures[p.path().filename().generic_string()].loadFromFile(p.path().generic_string());
		else
		{
			g_logger.Log("[ERROR] Could not load texture '%s'", p.path().filename().generic_string().c_str());
			return false;
		}*/
		textures[p.path().filename().generic_string()] = new sf::Texture();
		if (!textures[p.path().filename().generic_string()]->loadFromFile(p.path().generic_string()))
		{
			g_logger.Log("[ERROR] Could not load texture '%s'", p.path().filename().generic_string().c_str());
			return false;
		}
	}

	//load sounds
	//load misc
	g_logger.Log("[INFO] Loaded %d assets.", textures.size());
	std::map<std::string, sf::Texture*>::iterator it = textures.begin();
	std::map<std::string, sf::Texture*>::iterator end = textures.end();
	for (; it != end; ++it)
	{
		g_logger.Log("[DEBUG] Asset: %s", it->first.c_str());
	}
	return true;
}
