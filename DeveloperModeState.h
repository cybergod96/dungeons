#pragma once
#include "State.h"
#include "Map.h"
#include "state_id.h"
class DeveloperModeState : public State
{
private:
	Dungeon *dungeon;
	Player *player;
	bool debug_mode, window_focus;
public:
	DeveloperModeState();
	~DeveloperModeState();
	void OnEnter(int previousStateId);
	void OnExit(int nextStateId);
	void OnSuspend(int pushedStateId);
	void OnResume(int poppedStateId);
	void OnDraw(sf::RenderTarget &target, bool suspended);
	void OnEvent(sf::Event &event, bool suspended);
	void OnUpdate(bool suspended);
};

