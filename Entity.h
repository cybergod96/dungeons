#pragma once
#include <SFML/Graphics.hpp>
#include "Object.h"
#include "VirtualFS.h"

#define DIR_UP 1
#define DIR_DOWN 2
#define DIR_LEFT 3
#define DIR_RIGHT 4

namespace Framework
{
	class Entity: public Object 
	{
	protected:
		sf::Sprite sprite;
		//sf::Texture texture;
		bool go_up, go_down, go_left, go_right;
	public:
		Entity();
		~Entity();
		sf::FloatRect GetBox() { return sf::FloatRect(position.x*32,position.y*32,width,height); }
		bool Init(VirtualFS *vfs, const char *file_name);
		virtual void onInit() = 0;
		virtual void onDeinit() = 0;
		virtual void onUpdate() = 0;
		virtual void onInput(const sf::Event::KeyEvent &key) = 0;
		virtual void onDraw(sf::RenderTarget &target) = 0;
		virtual void onCollision(Object *other) = 0;
		virtual void SetPosition(Coord pos);
		void SetMovement(bool up, bool down, bool left, bool right);
		void SetMovement(int dir, bool b);
		bool CanGo(int dir);
	};
}

