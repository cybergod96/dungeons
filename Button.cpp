#include "Button.h"

Button::Button()
{
	//kod
}
Button::Button(sf::Vector2f pos, sf::Vector2f size, sf::String txt, sf::Font &f, sf::Color fillColor, sf::Color textColor, unsigned char_size)
{
	shape = sf::RectangleShape(size);
	shape.setPosition(pos.x,pos.y);
	//setSize(size);
	shape.setFillColor(fillColor);

	//text = sf::Text();
	text.setFont(f);
	text.setCharacterSize(DEFAULT_TEXT_SIZE);
	text.setString(txt);
	text.setCharacterSize(char_size);
	text.setColor(textColor);
	/*text.setOrigin(0,0);*/
	//text.setStyle(sf::Text::Regular);
	//text.setStyle(sf::Text::Style::Bold);

	/*text.setOrigin(text.getLocalBounds().left + text.getLocalBounds().width/2.0f,
					text.getLocalBounds().top + text.getLocalBounds().height/2.0f);*/

	//float x = shape.getPosition().x + (shape.getGlobalBounds().width - text.getGlobalBounds().width)/3;
	//float y = shape.getPosition().y + (shape.getGlobalBounds().height - text.getGlobalBounds().height)/3;

	//float x = shape.getPosition().x + ( (shape.getLocalBounds().width - text.getLocalBounds().width)/2 );
	//float y = shape.getPosition().y + ( (shape.getLocalBounds().height - text.getLocalBounds().height)/2 );

	//float x = shape.getPosition().x + shape.getLocalBounds().width/4.0f - text.getLocalBounds().width/2.75f;
	//float y = shape.getPosition().y + (shape.getLocalBounds().height/2.0f - text.getLocalBounds().height);

	//float x = shape.getPosition().x/* + ( shape.getLocalBounds().width / 2.0f - text.getLocalBounds().width / 2.0f )*/;
	//float y = shape.getPosition().y;

	float x = shape.getPosition().x + ( shape.getLocalBounds().width - text.getLocalBounds().width)/2;
	float y = shape.getPosition().y + ( shape.getLocalBounds().height - text.getLocalBounds().height)/4;

	text.setPosition(x,y);

	mouseOver = mouseOut = clicked = checkable = checked = false;
	visible = true;
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(shape);
	target.draw(text);
}

void Button::HandleEvents(sf::Event &e)
{
	/*if(e.type == sf::Event::MouseMoved)
	{
		if(getLocalBounds().contains(e.mouseMove.x,e.mouseMove.y))
		{
			mouseOver = true;
			mouseOut = false;
		}
		else
		{
			mouseOver = false;
			mouseOut = true;
		}
	}
	else if(e.type == sf::Event::MouseButtonPressed)
	{
		if(getLocalBounds().contains(e.mouseMove.x,e.mouseMove.y))
		{

		}
	}*/

	/*switch(e.type)
	{
	case sf::Event::MouseMoved:
		if(shape.getGlobalBounds().contains(e.mouseMove.x,e.mouseMove.y))
		{
			mouseOver = true;
			mouseOut = false;
		}
		else
		{
			mouseOver = false;
			mouseOut = true;
		}
		break;

	case sf::Event::MouseButtonPressed:
		if(mouseOver)
		{
			if(checkable) checked = true;
			else clicked = true;
		}
		break;

	case sf::Event::MouseButtonReleased:
		if(!checkable) clicked = false;
		break;
	}*/

	if(e.type == sf::Event::MouseMoved)
	{
		if(shape.getGlobalBounds().contains(e.mouseMove.x,e.mouseMove.y))
		{
			mouseOver = true;
			mouseOut = false;
		}
		else
		{
			mouseOver = false;
			mouseOut = true;
		}
	}
	else if(e.type == sf::Event::MouseButtonPressed)
	{
		if(mouseOver)
		{
			/*if(checkable) 
				checked = true;
			else */
				clicked = true;
		}
	}
	else if(e.type == sf::Event::MouseButtonReleased)
	{
		/*if(!checkable) */clicked = false;
	}
}

void Button::Logic(sf::Event &e)
{
	if(e.type == sf::Event::MouseMoved)
	{
		if(shape.getGlobalBounds().contains(e.mouseMove.x,e.mouseMove.y))
		{
			mouseOver = true;
			mouseOut = false;
		}
		else
		{
			mouseOver = false;
			mouseOut = true;
		}
	}
	else if(e.type == sf::Event::MouseButtonPressed)
	{
		if(mouseOver)
		{
			/*if(checkable) 
				checked = true;
			else */
				clicked = true;
		}
	}
	else if(e.type == sf::Event::MouseButtonReleased)
	{
		/*if(!checkable) */clicked = false;
	}
}

bool Button::Clicked(sf::Event &e)
{
	if(mouseOver)
	{
		if(e.type == sf::Event::MouseButtonPressed)
		{
			if(mouseOver)
			{
				/*if(checkable) 
					checked = true;
				else */
					return true;
			}
		}
		else if(e.type == sf::Event::MouseButtonReleased)
		{
			/*if(!checkable) */return false;
		}
	}
	return false;
}