#include "Entity.h"


Framework::Entity::Entity()
{
	
}


Framework::Entity::~Entity()
{
}

bool Framework::Entity::Init(VirtualFS * vfs, const char *file_name)
{

	return true;
}

void Framework::Entity::onInit()
{
}

void Framework::Entity::onDeinit()
{
}

void Framework::Entity::onUpdate()
{
}

void Framework::Entity::onInput(const sf::Event::KeyEvent &key)
{
}

void Framework::Entity::onDraw(sf::RenderTarget &target)
{
	target.draw(sprite);
}

void Framework::Entity::onCollision(Object *other)
{
	return;
}

void Framework::Entity::SetPosition(Coord pos)
{
	position = pos; 
	sprite.setPosition(sf::Vector2f(pos.x * 32, pos.y * 32));
}

void Framework::Entity::SetMovement(bool up, bool down, bool left, bool right) 
{ 
	go_up = up; 
	go_down = down; 
	go_left = left; 
	go_right = right; 
}

void Framework::Entity::SetMovement(int dir, bool b)
{
	switch (dir)
	{
	case 1:
		go_up = b;
		break;
	case 2:
		go_down = b;
		break;
	case 3:
		go_left = b;
		break;
	case 4:
		go_right = b;
		break;
	}
}

bool Framework::Entity::CanGo(int dir)
{
	switch (dir)
	{
	case DIR_UP:
		return go_up;
		break;
	case DIR_DOWN:
		return go_down;
		break;
	case DIR_LEFT:
		return go_left;
		break;
	case DIR_RIGHT:
		return go_right;
		break;
	}
}
