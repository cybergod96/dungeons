#pragma once
struct Coord
{
	int x, y;

	Coord() { x = y = 0; };
	Coord(int x, int y) { this->x = x; this->y = y; }
	inline bool operator==(const Coord &c) { return x == c.x && y == c.y; }
};