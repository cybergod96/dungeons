#include "HealthBar.h"

HealthBar::HealthBar(sf::Vector2f pos, int value, int maxl, int maxv, sf::Color color)
{
	cur_value = value;
	max_length = maxl;
	max_value = maxv;
	shape.setFillColor(color);
	shape.setPosition(pos.x,pos.y);
	Update();
}


void HealthBar::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(shape,states);
}

HealthBar::~HealthBar(void)
{
}

void HealthBar::Update()
{
	if(cur_value > max_value) cur_value = max_value;
	if(cur_value < 0) cur_value = 0;
	shape.setSize(sf::Vector2f((max_length*cur_value)/max_value,DEFAULT_BAR_HEIGHT));
}
