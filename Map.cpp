#include "Map.h"

Map::Map()
{
	for (int i = 0; i < MAP_WIDTH; i++)
	{
		for (int j = 0; j < MAP_HEIGHT; j++)
		{
			tiles[i][j].type = EMPTY;
			tiles[i][j].visited = false;
		}
	}
}

Map::~Map()
{
	//std::cout << "Calling ~Map()\n";
	for (int i = 0; i < objects.size(); i++)
	{
		objects[i]->onDeinit();
		if(objects[i]->GetObjectType() != Framework::ObjectType::Object_Player)
			delete objects[i];
	}
	objects.clear();
}

bool Map::CreateSegment(DIRECTION dir, Segment & parent, Coord * door_pos, Segment * destiny)
{
	Coord  door;
	Segment result{};
	DIRECTION my_direction = dir;
	if(randomInt(101)<= ROOM_CHANCE)//make room
	{		
		result.type = SEGMENT_TYPE::ROOM;
		result.width = randomInt(MIN_ROOM_SIZE, MAX_ROOM_SIZE)+2;
		result.height = randomInt(MIN_ROOM_SIZE, MAX_ROOM_SIZE)+2;

		if (dir == DIRECTION::UP)//na p�noc od rodzica
		{
			if (!parent.north) return false;
			result.pos.x = parent.pos.x;
			result.pos.y = parent.pos.y - result.height + 1;
			parent.north = result.south = false;

			door.y = parent.pos.y;

			if (result.width > parent.width)
				door.x = randomInt(parent.pos.x + 1, parent.pos.x + (parent.width - 2));
			else
				door.x = randomInt(result.pos.x + 1, result.pos.x + (result.width - 2));

		}
		else if (dir == DIRECTION::DOWN)//na po�udnie od rodzica
		{
			if (!parent.south) return false;
			result.pos.x = parent.pos.x;
			result.pos.y = parent.pos.y + parent.height - 1;
			parent.south = result.north = false;

			door.y = result.pos.y;

			if (result.width > parent.width)
				door.x = randomInt(parent.pos.x + 1, parent.pos.x + (parent.width - 2));
			else
				door.x = randomInt(result.pos.x + 1, result.pos.x + (result.width - 2));
		}
		else if (dir == DIRECTION::LEFT)//na zach�d od rodzica
		{
			if (!parent.west) return false;
			result.pos.x = parent.pos.x - result.width + 1;
			result.pos.y = parent.pos.y;
			parent.west = result.east = false;

			door.x = parent.pos.x;

			if (result.height > parent.height)
				door.y = randomInt(parent.pos.y + 1, parent.pos.y + (parent.height - 2));
			else
				door.y = randomInt(result.pos.y + 1, result.pos.y + (result.height - 2));
		}
		else if (dir == DIRECTION::RIGHT)//na wsch�d od rodzica
		{
			if (!parent.east) return false;
			result.pos.x = parent.pos.x + parent.width - 1;
			result.pos.y = parent.pos.y;
			parent.east = result.west = false;

			door.x = result.pos.x;

			if (result.height > parent.height)
				door.y = randomInt(parent.pos.y + 1, parent.pos.y + (parent.height - 2));
			else
				door.y = randomInt(result.pos.y + 1, result.pos.y + (result.height - 2));

		}
	}
	else //make corridor
	{
		result.type = SEGMENT_TYPE::CORRIDOR;
		//if (parent.type == SEGMENT_TYPE::CORRIDOR) return false;
		if (dir == DIRECTION::UP)
		{
			if (!parent.north) return false;

			result.width = 3;
			result.height = randomInt(MIN_CORRIDOR_LENGTH, MAX_CORRIDOR_LENGTH) + 2;

			result.pos.x = randomInt(parent.pos.x /*+ 1*/, parent.pos.x + parent.width - 3/*3*/);
			result.pos.y = parent.pos.y - result.height + 1;

			parent.north = result.south = false;
			result.east = result.west = false;

			door.y = parent.pos.y;
			door.x = /*parent.type == SEGMENT_TYPE::CORRIDOR ? */result.pos.x+1 /*: result.pos.x + 1*/;
		}
		else if (dir = DIRECTION::DOWN)
		{
			if (!parent.south) return false;
			
			result.width = 3;
			result.height = randomInt(MIN_CORRIDOR_LENGTH, MAX_CORRIDOR_LENGTH) + 2;


			result.pos.x = randomInt(parent.pos.x /*+ 1*/, parent.pos.x + parent.width - 3/*3*/);
			result.pos.y = parent.pos.y + parent.height - 1;

			parent.south = result.north = false;
			result.east = result.west = false;

			door.y = result.pos.y;
			door.x = /*parent.type == SEGMENT_TYPE::CORRIDOR ? */result.pos.x+1 /*: result.pos.x + 1*/;
		}
		else if (dir == DIRECTION::LEFT)
		{
			if (!parent.west) return false;

			result.height = 3;
			result.width = randomInt(MIN_CORRIDOR_LENGTH, MAX_CORRIDOR_LENGTH) + 2;

			result.pos.x = parent.pos.x - result.width + 1;
			result.pos.y = randomInt(parent.pos.y + 1, parent.pos.y + parent.height - 2);

			parent.west = result.east = false;
			result.north = result.south = false;

			door.x = parent.pos.x;
			door.y = result.pos.y + 1;
		}
		else if (dir == DIRECTION::RIGHT)
		{
			if (!parent.east) return false;

			result.height = 3;
			result.width = randomInt(MIN_CORRIDOR_LENGTH, MAX_CORRIDOR_LENGTH) + 2;

			result.pos.x = parent.pos.x + parent.width - 1;
			result.pos.y = randomInt(parent.pos.y + 1, parent.pos.y + parent.height - 2);

			parent.east = result.west = false;
			result.north = result.south = false;

			door.x = result.pos.x;
			door.y = result.pos.y + 1;
		}

	}
	if (!CheckSegment(result, dir))
	{
		//std::cout << result.DebugInfo() << std::endl;
		return false;
	}

	*destiny = result;
	*door_pos = door;
	//std::cout << result.DebugInfo() << std::endl;
	return true;
}

void Map::Generate(int depth)
{
	int fw = randomInt(MIN_ROOM_SIZE, MAX_ROOM_SIZE), fh = randomInt(MIN_ROOM_SIZE, MAX_ROOM_SIZE);
	Segment pierwszy = Segment( ROOM,Coord(MAP_WIDTH/2-fw/2,MAP_HEIGHT/2-fh/2),fw,fh );
	PlaceSegment(pierwszy);
	rooms.push_back(pierwszy);

	//generate dungeon
	for (int i = 0; i < MAX_SEGMENTS; i++)
	{
		Segment new_segment;
		Coord door;
		if (CreateSegment(static_cast<DIRECTION>(randomInt(4)), rooms[randomInt(rooms.size())], &door, &new_segment))
		{
			PlaceSegment(new_segment);
			SetTile(door, DOOR);
			rooms.push_back(new_segment);
		}
	}
	
	start_pos = { rooms[0].pos.x + rooms[0].width / 2,rooms[0].pos.y + rooms[0].height / 2 };
	int idx;
	do
	{
		idx = randomInt(rooms.size());

	} while (rooms[idx].type == SEGMENT_TYPE::CORRIDOR);
	stairs_down = { rooms[idx].pos.x + rooms[idx].width / 2,rooms[idx].pos.y + rooms[idx].height / 2 };
	SetTile(stairs_down, STAIRS_DOWN);
	SetTile(start_pos, STAIRS_UP);

	//TODO: generate enemies
	int enemies_num = randomInt(1, 12);
	for(int i = 0; i < enemies_num; i++)
	{
		int room = randomInt(rooms.size());
		InsertObject(new DummyEnemy(rooms[room].RandomPosInside()));
	}
	
	//generate chests
	//generate traps

}

bool Map::CheckSegment(const Segment &s, const DIRECTION &dir)
{
	//czy wykracza poza granice mapy
	if (s.pos.x < 0 || s.pos.y < 0 || s.pos.x >= MAP_WIDTH || s.pos.y >= MAP_HEIGHT
		|| (s.pos.x + s.width) >= MAP_WIDTH || (s.pos.y + s.height) >= MAP_HEIGHT)
	{
		//std::cout << "Wychodzi poza mape!";
		return false;
	}
	//czy wchodzi na jaki� segment
	if (dir == DIRECTION::UP)
	{
		for (int i = s.pos.x; i < (s.pos.x + s.width); i++)
		{
			for (int j = s.pos.y; j < (s.pos.y + s.height - 1); j++)
			{
				if (tiles[i][j].visited)
					return false;
			}
		}
	}
	else if (dir == DIRECTION::DOWN)
	{
		for (int i = s.pos.x; i < (s.pos.x + s.width); i++)
		{
			for (int j = s.pos.y + 1; j < (s.pos.y + s.height); j++)
			{
				if (tiles[i][j].visited)
					return false;
			}
		}
	}
	else if (dir == DIRECTION::LEFT)
	{
		for (int i = s.pos.x; i < (s.pos.x + s.width - 1); i++)
		{
			for (int j = s.pos.y + 1; j < (s.pos.y + s.height); j++)
			{
				if (tiles[i][j].visited)
				{
					return false;
				}
			}
		}
	}
	else if (dir == DIRECTION::RIGHT)
	{
		for (int i = s.pos.x + 1; i < (s.pos.x + s.width); i++)
		{
			for (int j = s.pos.y + 1; j < (s.pos.y + s.height); j++)
			{
				if (tiles[i][j].visited)
					return false;
			}
		}
	}
	return true;
}

void Map::PlaceSegment(const Segment &s)
{
	for (int i = 0; i < s.width; i++)
	{
		for (int j = 0; j < s.height; j++)
		{
			if (i == 0 || i == s.width - 1 || j == 0 || j == s.height - 1)
				SetTile(i + s.pos.x, j + s.pos.y, TILE_TYPE::WALL);
			else
				SetTile(i + s.pos.x, j + s.pos.y, TILE_TYPE::FLOOR);
		}
	}
}

void Map::Objects_Update()
{
	//call onUpdate for all objects
	for (int i = 0; i < objects.size(); i++)
	{
		objects[i]->onUpdate();
	}

	//check collisions
	for (int i = 0; i < objects.size(); i++)
	{
		for (int j = 0; j < objects.size(); j++)
		{
			if (i != j)
			{
				if (objects[i]->GetRect().intersects(objects[j]->GetRect()))
				{
					objects[i]->onCollision(objects[j]);
					objects[j]->onCollision(objects[i]);
				}
			}
		}
	}

	//destroy objects with alive==false;
	for (int i = objects.size()-1; i >=0; i--)
	{
		if(!objects[i]->IsAlive())
		{
			objects[i]->onDeinit();
			delete objects[i];
			objects.erase(objects.begin() + i);
		}
	}
}

void Map::Objects_HandleInput(const sf::Event &e)
{
	//TODO: mo�liwe, �e trzeba b�dzie poprawi�
	for (int i = 0; i < objects.size(); i++)
	{
		objects[i]->onInput(e);
	}
}

void Map::Objects_Draw(sf::RenderTarget & target)
{
	for (int i = 0; i < objects.size(); i++)
	{
		objects[i]->onDraw(target);
	}
}

Dungeon::Dungeon(Player *player)
{
	depth = 1;
	levels.emplace_back(new Map());
	levels[0]->Generate(depth);
	this->player = player;//player = new Player();
	levels[0]->InsertObject(player);
	player->SetPosition(levels[0]->GetStartPos());
	SetEntitiesMovement();
	camera = sf::View(sf::FloatRect(MAP_WIDTH * 16 - SCREEN_WIDTH / 2, MAP_HEIGHT * 16 - SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT));
	LookAtPlayer();
}

Dungeon::~Dungeon()
{
	for (int i = 0; i < levels.size(); i++)
		delete levels[i];
	levels.clear();
	//delete player;
}

void Dungeon::SetMovement(Framework::Entity *ent)
{
	bool u, d, l, r;
	Coord pos = ent->GetPosition();
	u = cur_level->GetTile(pos.x, pos.y - 1) != TILE_TYPE::WALL;
	d = cur_level->GetTile(pos.x, pos.y + 1) != TILE_TYPE::WALL;
	l = cur_level->GetTile(pos.x - 1, pos.y) != TILE_TYPE::WALL;
	r = cur_level->GetTile(pos.x + 1, pos.y) != TILE_TYPE::WALL;
	ent->SetMovement(u, d, l, r);
}

void Dungeon::SetEntitiesMovement()
{
	//bool u, d, l, r;
	Coord pos, pos2;
	Framework::Entity *ent, *ent2;
	//float dist;
	for (int i = 0; i < cur_level->objects.size(); i++)
	{
		if (cur_level->objects[i]->GetObjectType() == Framework::ObjectType::Object_Enemy ||
			cur_level->objects[i]->GetObjectType() == Framework::ObjectType::Object_Player)
		{
			//SetMovement(dynamic_cast<Framework::Entity *>(cur_level->objects[i]));
			ent = dynamic_cast<Framework::Entity *>(cur_level->objects[i]);
			pos = ent->GetPosition();
			//check collision with walls
			ent->SetMovement(DIR_UP, cur_level->GetTile(pos.x, pos.y - 1) != TILE_TYPE::WALL);
			ent->SetMovement(DIR_DOWN, cur_level->GetTile(pos.x, pos.y + 1) != TILE_TYPE::WALL);
			ent->SetMovement(DIR_LEFT, cur_level->GetTile(pos.x - 1, pos.y) != TILE_TYPE::WALL);
			ent->SetMovement(DIR_RIGHT, cur_level->GetTile(pos.x + 1, pos.y) != TILE_TYPE::WALL);

			//check collision with other entities
			for (int j = 0; j < cur_level->objects.size(); j++)
			{
				if (i != j)
				{
					if (cur_level->objects[j]->GetObjectType() == Framework::ObjectType::Object_Enemy ||
						cur_level->objects[j]->GetObjectType() == Framework::ObjectType::Object_Player)
					{
						ent2 = dynamic_cast<Framework::Entity *>(cur_level->objects[j]);
						pos2 = ent2->GetPosition();
						//dist = ent->GetDistance(ent2);
						if(ent->GetDistance(ent2) == 1)
						{
							if (pos.x == pos2.x)
							{
								if (pos.y + 1 == pos2.y)
								{
									ent->SetMovement(DIR_DOWN, false);
									ent2->SetMovement(DIR_UP, false);
								}
								else if (pos.y - 1 == pos2.y)
								{
									ent->SetMovement(DIR_UP, false);
									ent2->SetMovement(DIR_DOWN, false);
								}
								else
								{
									ent->SetMovement(DIR_UP, ent->CanGo(DIR_UP));
									ent->SetMovement(DIR_DOWN, ent->CanGo(DIR_DOWN));
									ent2->SetMovement(DIR_UP, ent2->CanGo(DIR_UP));
									ent2->SetMovement(DIR_DOWN, ent2->CanGo(DIR_DOWN));
								}
							}
							else if (pos.y == pos2.y)
							{
								if (pos.x + 1 == pos2.x)
								{
									ent->SetMovement(DIR_RIGHT, false);
									ent2->SetMovement(DIR_LEFT, false);
								}
								else if (pos.x - 1 == pos2.x)
								{
									ent->SetMovement(DIR_LEFT, false);
									ent2->SetMovement(DIR_RIGHT, false);
								}
								else
								{
									ent->SetMovement(DIR_LEFT, ent->CanGo(LEFT));
									ent->SetMovement(DIR_RIGHT, ent->CanGo(DIR_RIGHT));
									ent2->SetMovement(DIR_LEFT, ent2->CanGo(DIR_LEFT));
									ent2->SetMovement(DIR_RIGHT, ent2->CanGo(DIR_RIGHT));
								}
							}
						}
					}
				}
			}
		}
	}
}

void Dungeon::HandleInput(const sf::Event & e)
{
	if (e.type == sf::Event::KeyPressed)
	{
		if (e.key.code == sf::Keyboard::Up)//move up
		{
			if(player->CanGo(DIR_UP))
			{
				player->SetPosition({ player->GetPosition().x,player->GetPosition().y - 1 });
				camera.move(0, -32);
				if (player->GetPosition() == levels[depth - 1]->stairs_down)
					Descend();
				else if (player->GetPosition() == levels[depth - 1]->start_pos)
					Ascend();
			}
			EnemiesTurn();
			SetEntitiesMovement();
		}
		else if (e.key.code == sf::Keyboard::Down)//move down
		{
			if(player->CanGo(DIR_DOWN))
			{
				player->SetPosition({ player->GetPosition().x,player->GetPosition().y + 1 });
				camera.move(0, 32);
				if (player->GetPosition() == levels[depth - 1]->stairs_down)
					Descend();
				else if (player->GetPosition() == levels[depth - 1]->start_pos)
					Ascend();
			}
			EnemiesTurn();
			SetEntitiesMovement();
		}
		else if (e.key.code == sf::Keyboard::Left)//move left
		{
			if(player->CanGo(DIR_LEFT))
			{
				player->SetPosition({ player->GetPosition().x - 1,player->GetPosition().y });
				camera.move(-32, 0);
				if (player->GetPosition() == levels[depth - 1]->stairs_down)
					Descend();
				else if (player->GetPosition() == levels[depth - 1]->start_pos)
					Ascend();
			}
			EnemiesTurn();
			SetEntitiesMovement();
		}
		else if (e.key.code == sf::Keyboard::Right)//move right
		{
			if(player->CanGo(DIR_RIGHT))
			{
				player->SetPosition({ player->GetPosition().x + 1,player->GetPosition().y });
				camera.move(32, 0);
				if (player->GetPosition() == levels[depth - 1]->stairs_down)
					Descend();
				else if (player->GetPosition() == levels[depth - 1]->start_pos)
					Ascend();
			}
			EnemiesTurn();
			SetEntitiesMovement();
		}
		else if (e.key.code == sf::Keyboard::Space)
		{
			LookAtPlayer();
		}
		else if (e.key.code == sf::Keyboard::F)
		{
			EnemiesTurn();
			SetEntitiesMovement();
		}
		if (e.key.code == sf::Keyboard::R) camera.setRotation(randomInt(181));
		if(e.key.code == sf::Keyboard::Z)
		{
			camera.zoom(1.5);
		}
		if (e.key.code == sf::Keyboard::X) camera.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		if (e.key.code == sf::Keyboard::H) player->Hurt(10);
	}
}

void Dungeon::EnemiesTurn()
{
	for (int i = 0; i < levels[depth - 1]->objects.size(); i++)
	{
		if (levels[depth - 1]->objects[i]->GetObjectType() == Framework::ObjectType::Object_Enemy)
		{
			dynamic_cast<DummyEnemy *>(levels[depth - 1]->objects[i])->Turn();
		}
	}
}

void Dungeon::Update()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) camera.move(0, -CAMERA_SPEED);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) camera.move(0, CAMERA_SPEED);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) camera.move(-CAMERA_SPEED, 0);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) camera.move(CAMERA_SPEED, 0);

	levels[depth - 1]->Objects_Update();
}

void Dungeon::LookAtPlayer()
{
	camera.setCenter(player->GetPosition().x * 32 - 16, player->GetPosition().y * 32 - 16);
}

void Dungeon::Descend()
{
	if (depth + 1 > levels.size())
	{
		levels.emplace_back(new Map());
		depth += 1;
		levels[depth - 1]->Generate(depth);
		levels[depth - 1]->InsertObject(player);
		player->SetPosition(levels[depth - 1]->GetStartPos());
		SetEntitiesMovement();
		LookAtPlayer();
	}
	else
	{
		depth += 1;
		player->SetPosition(levels[depth - 1]->GetStartPos());
		SetEntitiesMovement();
		LookAtPlayer();
	}
}

void Dungeon::Ascend()
{
	if(depth > 1) 
	{
		depth -= 1;
		player->SetPosition(levels[depth - 1]->GetStairsDownPos());
		SetEntitiesMovement();
		LookAtPlayer();
	}
}

void Dungeon::DrawDungeon(sf::RenderTarget &target, bool debug)
{
	target.setView(camera);
	sf::FloatRect cam_rect(camera.getCenter().x - SCREEN_WIDTH / 2, camera.getCenter().y - SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT);
	for (int i = 0; i < MAP_WIDTH; i++)
	{
		for (int j = 0; j < MAP_HEIGHT; j++)
		{
			sf::FloatRect tile_rect(i*32,j*32,32,32);
			if(/*cam_rect.intersects(tile_rect)*/true)
			{
				if (levels[depth - 1]->tiles[i][j].type != EMPTY)
				{
					tileset.setTextureRect(tile_rects[levels[depth - 1]->tiles[i][j].type]);
					tileset.setPosition(i * 32, j * 32);
					target.draw(tileset);
				}

			}
		}
		levels[depth - 1]->Objects_Draw(target);
		player->onDraw(target);
	}
	target.setView(target.getDefaultView());
}

void Dungeon::LoadTileset()
{
	//sf::Texture tex = ;
	tileset = sf::Sprite(g_assetManager.GetTexture("tileset.png"));
	tileset.setTextureRect(sf::IntRect(0, 0, 32, 32));
}
