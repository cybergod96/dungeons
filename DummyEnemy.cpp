#include "DummyEnemy.h"



DummyEnemy::DummyEnemy(Coord pos)
{
	/*shape.setSize(sf::Vector2f(32, 32));
	shape.setFillColor(sf::Color::Red);*/
	//texture.loadFromFile("enemy.png");
	sprite.setTexture(g_assetManager.GetTexture("enemy.png"));
	width = /*shape*/sprite.getLocalBounds().width;
	height = /*shape*/sprite.getLocalBounds().height;
	SetPosition(pos);
}


DummyEnemy::~DummyEnemy()
{
}

void DummyEnemy::onUpdate()
{
}

void DummyEnemy::onDeinit()
{
}

void DummyEnemy::onInit()
{
}

void DummyEnemy::onInput(const sf::Event & e)
{
}

void DummyEnemy::onDraw(sf::RenderTarget &target)
{
	//target.draw(shape);
	target.draw(sprite);
}

void DummyEnemy::onCollision(Object * other)
{
}

void DummyEnemy::onInput(const sf::Event::KeyEvent & key)
{
}

void DummyEnemy::onDestroy()
{
}

void DummyEnemy::SetPosition(Coord pos)
{ 
	position = pos; 
	/*shape*/sprite.setPosition(sf::Vector2f(pos.x * 32, pos.y * 32));
}

void DummyEnemy::Turn()
{
	switch (randomInt(4))
	{
	case 0:
		if (go_up) 
			SetPosition(Coord(position.x, position.y - 1));
		break;
	case 1:
		if(go_down)
			SetPosition(Coord(position.x, position.y + 1));
		break;
	case 2:
		if(go_left)
			SetPosition(Coord(position.x - 1, position.y));
		break;
	case 3:
		if(go_right)
			SetPosition(Coord(position.x + 1, position.y));
		break;
	}

}
