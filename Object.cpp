#include "Object.h"

Framework::Object::Object()
{
	alive = true;
}


Framework::Object::~Object()
{
	alive = false;
}

float Framework::Object::GetDistance(Framework::Object *other)
{
	int deltaX = other->position.x - position.x, deltaY = other->position.y - position.y;
	return sqrt(deltaX*deltaX + deltaY*deltaY);
}