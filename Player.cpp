#include "Player.h"



void Player::LoadResources()
{
	/*if (!texture.loadFromFile("player2.png"))
		return false;*/
	sprite.setTexture(g_assetManager.GetTexture("player2.png"));
	width = sprite.getLocalBounds().width;
	height = sprite.getLocalBounds().height;
}

Player::Player()
{
	
}


Player::~Player()
{
}

void Player::onInit()
{
	//width = sprite.getLocalBounds().width;
	//height = sprite.getLocalBounds().height;
	//texture.loadFromFile("player2.png");
	//sprite.setTexture(texture);
	/*shape.setSize(sf::Vector2f(32, 32));
	shape.setFillColor(sf::Color::Green);*/
	//width = sprite/*shape*/.getLocalBounds().width;
	//height = sprite/*shape*/.getLocalBounds().height;
	go_up = go_down = go_left = go_right = true;
	/*hp = 100;*/
	health_bar = HealthBar(sf::Vector2f(0, 0), 100, 100, 100, sf::Color::Red);;
}

void Player::onDeinit()
{
	return;
}

void Player::onCollision(Object * other)
{
	return;
}

void Player::onDestroy()
{
	return;
}

void Player::onInput(const sf::Event &e)
{
	//if (e.type == sf::Event::KeyPressed)
	//{
	//	if (e.key.code == sf::Keyboard::W && go_up)//move up
	//	{
	//		SetPosition({ GetPosition().x,GetPosition().y - 1 });
	//	}
	//	else if (e.key.code == sf::Keyboard::S && go_down)//move down
	//	{
	//		SetPosition({ GetPosition().x,GetPosition().y + 1 });
	//	}
	//	else if (e.key.code == sf::Keyboard::A && go_left)//move left
	//	{
	//		SetPosition({ GetPosition().x - 1,GetPosition().y });
	//	}
	//	else if (e.key.code == sf::Keyboard::D && go_right)//move right
	//	{
	//		SetPosition({ GetPosition().x,GetPosition().y - 1 });
	//	}
	//}
}

void Player::onDraw(sf::RenderTarget & target)
{
	target.draw(sprite);
	target.draw(health_bar);
	//target.draw(shape);
}

void Player::onUpdate()
{
	/*position.x = sprite.getPosition().x;
	position.x = sprite.getPosition().y;*/
}

void Player::SetPosition(Coord pos)
{
	position = pos;
	sprite.setPosition(sf::Vector2f(pos.x * 32, pos.y * 32));
	//shape.setPosition(sf::Vector2f(pos.x * 32, pos.y * 32));
}


