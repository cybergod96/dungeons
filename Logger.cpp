#include "Logger.h"

Logger::Logger()
{
	file.open(GetLoadingPath("log.txt"), std::ios::out);
}


Logger::~Logger()
{
	file.close();
}

void Logger::Log(const char *format, ...)
{
	//http://www.cplusplus.com/reference/cstdio/vsprintf/
	char buffer[256];
	va_list args;
	va_start(args, format);
	vsprintf(buffer, format, args);
	file << buffer << std::endl;
	va_end(args);
}
