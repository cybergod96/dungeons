#include "VirtualFS.h"

///<summary>
///Load file with packed data.
///</summary>
Framework::VirtualFS::VirtualFS(string file_name)
{
	//open file
	file.open(file_name, ios::in | ios::binary);
	if (!file.good()) throw VFSE_FILE_ERROR;
	else
	{
		VFS_HEADER header;
		char *tmp_header = new char[sizeof(VFS_HEADER)];

		file.read(tmp_header, sizeof(VFS_HEADER));
		header = *(VFS_HEADER*)tmp_header;
		if (header.version != VFS_VERSION) throw VFSE_VERSION_ERROR;

		for (int i = 0; i < header.files_count; i++)
		{
			string name;
			streampos size, offset;

			char *tmp_name = new char[ASSET_NAME_LENGTH + 1];
			file.read(tmp_name, ASSET_NAME_LENGTH);
			stringstream ss;
			ss << tmp_name;
			name = ss.str();

			char *tmp_size = new char[sizeof(streampos)];
			file.read(tmp_size, sizeof(streampos));
			size = *(streampos*)tmp_size;

			char *tmp_offset = new char[sizeof(streampos)];
			file.read(tmp_offset, sizeof(streampos));
			offset = *(streampos*)tmp_offset;

			files_table.push_back(VFS_FILEINFO(name, offset, size));
		}
	}
}

Framework::VirtualFS::~VirtualFS()
{
	if (file.good()) file.close();
}

///<summary>
///Get packed file content.
///</summary>
Framework::VFS_ExtractedFile Framework::VirtualFS::GetFile(string name)
{
	for (unsigned i = 0; i < files_table.size(); i++)
	{
		if (files_table[i].name == name)
		{
			//char *f = new char[files_table[i].size];
			//file.read(f, files_table[i].size);
			//memcpy(extracted.data, f, files_table[i].size);
			VFS_ExtractedFile extracted{ new char[files_table[i].size],files_table[i].size };
			file.seekg(files_table[i].offset);
			//extracted.data = new char [files_table[i].size];
			file.read(extracted.data, extracted.size);
			return extracted;
		}
	}

	return VFS_ExtractedFile{ NULL,0 };
}

/*

///<summary>
///Extract file with specified name to given path.
///</summary>
bool VirtualFS::ExtractFile(string file_name, string path)
{
	for (unsigned i = 0; i < files_table.size(); i++)
	{
		if (files_table[i].name == file_name)
		{
			char *f = new char[files_table[i].size];
			file.seekg(files_table[i].offset);
			file.read(f, files_table[i].size);
			stringstream ss;
			ss << path << "\\" << file_name;
			ofstream out(ss.str(), ios::binary);
			if (!out.good()) return false;
			out.write(f, files_table[i].size);
			out.close();
		}
	}
	return true;
}

///<summary>
///Extract all packed files to given path.
///</summary>
bool VirtualFS::ExtractAllFiles(string path)
{
	for (unsigned i = 0; i < files_table.size(); i++)
	{
		stringstream p;
		p <<path<< files_table[i].name<< ".bin";
		if (!ExtractFile(files_table[i].name, p.str())) return false;
	}

	return true;
}

*/