#include "main.h"

int main(int argc, char **argv)
{
	g_logger.Log("[INFO] Working Directory: %s", GetLoadingPath(""));
	g_logger.Log("[INFO] Creating window (v-sync on)...");
	window.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Dungeons", sf::Style::Titlebar | sf::Style::Close);
	window.setVerticalSyncEnabled(true);
	g_logger.Log("[INFO] Setting FPS limit: %d...",FPS_LIMIT);
	window.setFramerateLimit(FPS_LIMIT);
	g_logger.Log("[INFO] Loading 'times.ttf'...");

	if(!font.loadFromFile(GetLoadingPath("times.ttf")))
	{
		g_logger.Log("[ERROR] Could not load 'times.ttf'.");
		return 1;
	}

	g_logger.Log("[INFO] Loading assets...");
	if (!g_assetManager.LoadAssets(GetLoadingPath("assets")))
		g_logger.Log("[ERROR] Failed to load required assets!");

	g_logger.Log("[INFO] Initializing state machine...");
	state_machine = new StateMachine();
	g_logger.Log("[INFO] Initializing state DeveloperMode...");
	state_machine->RegisterState(STATE_DEVELOPER_MODE, new DeveloperModeState());
	g_logger.Log("[INFO] Pushing state DeveloperMode...");
	state_machine->PushState(STATE_DEVELOPER_MODE);
	g_logger.Log("[INFO] Initialization done");
	g_logger.Log("[INFO] Working Directory: %s",GetLoadingPath(""));

    while (window.isOpen())
    {
        while (window.pollEvent(event))
        {
			state_machine->OnEvent(event);
			if (event.type == sf::Event::Closed)
                window.close();	
        }

		state_machine->OnUpdate();
	
		window.clear();
		state_machine->OnDraw(window);
        window.display();
    }

	delete state_machine;
    return 0;
}