#pragma once
#include <SFML/Graphics.hpp>
#include <map>
#include <filesystem>
#include "Singleton.h"
#include "Logger.h"

//TODO: zaimplementowa� klas� AssetManager
class AssetManager : public Framework::Singleton<AssetManager>
{
private:
	std::map<std::string, sf::Texture*> textures;
public:
	AssetManager();
	~AssetManager();
	sf::Texture &GetTexture(std::string name);
	bool LoadAssets(std::string path);
};

#define g_assetManager AssetManager::Instance()

