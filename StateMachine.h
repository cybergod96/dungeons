#pragma once
#include "State.h"
#include "Logger.h"
#include <vector>
#include <map>

//TODO: usun�� using namespace std

using namespace std;

class StateMachine
{
private:
	vector<State *>states;
	map<int, State*>registered_states;
public:
	void RegisterState(int id, State *state); //ok
	bool IsRegistered(int id) const; //ok
	bool IsRegistered(State *state); //ok
	void PushState(int id); //ok
	void PopState(); //ok
	const int *StateId(State *state); //ok
	State *GetRegisteredState(int id); //ok
	void PopAllStates() { states.clear(); } //ok
	StateMachine();
	~StateMachine();
	void OnUpdate(); //ok
	void OnDraw(sf::RenderTarget &target); //ok
	void OnEvent(sf::Event &event); //ok
};

