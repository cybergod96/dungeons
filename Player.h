#pragma once
#include "Object.h"
#include "Entity.h"
#include "HealthBar.h"
#include "AssetManager.h"
class Player: public Framework::Entity
{
private:
	//int hp;
	HealthBar health_bar;
	//sf::Sprite sprite;
	//sf::Texture texture;
	//sf::RectangleShape shape;
	//std::vector<Item *>equipment;
	//Item *weapon;
	//Item *armor;
public:
	inline void Hurt(int dmg) { /*hp -= dmg;*/ health_bar.Subtract(dmg); }
	inline int GetHP() { return health_bar.GetValue();/*hp*/; }
	void onInit();
	void onDeinit();
	void onCollision(Object *other);
	void onDestroy();
	void onInput(const sf::Event &e);
	void onDraw(sf::RenderTarget &target); //TODO: doda� parametr cam_rect
	void onUpdate();
	void onInput(const sf::Event::KeyEvent &key) {};
	void SetPosition(Coord pos);
	sf::FloatRect GetRect() { return /*shape*/sprite.getGlobalBounds(); }
	Framework::ObjectType GetObjectType() { return Framework::ObjectType::Object_Player; }
	
	void LoadResources();

	Player();
	~Player();
};

