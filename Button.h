#pragma once

#include <SFML/Graphics.hpp>

#define DEFAULT_TEXT_SIZE 20

class Button: public sf::Drawable
{
private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	sf::RectangleShape shape;
	sf::Text text;
	bool mouseOver, mouseOut, clicked, visible, checkable, checked;
public:
	void HandleEvents(sf::Event &e);
	void Logic(sf::Event &e);
	Button(sf::Vector2f pos, sf::Vector2f size, sf::String txt, sf::Font &f, sf::Color fillColor, sf::Color textColor, unsigned char_size);
	Button();
	void SetVisible(bool isVisible) {visible = isVisible;}
	void SetCheckable(bool isCheckable) {checkable = isCheckable;}
	bool MouseOver() {return mouseOver;}
	bool MouseOut() {return mouseOut;}
	bool Clicked(sf::Event &e);
	bool Checked() {return checked;}
	void SetCharSize(unsigned s) { text.setCharacterSize(s); }
};