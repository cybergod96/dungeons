#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include <cmath>
#include "Coord.h"

namespace Framework
{
	enum class ObjectType {Object_Base, Object_Player, Object_Enemy, Object_Chest, Object_Item};
	class Object
	{
	private:
		//sf::IntRect rect;
		bool alive;
	protected:
		Coord position;
		int width, height;
	public:
		Object();
		inline Coord GetPosition() { return position; }
		inline virtual sf::FloatRect GetRect() { return sf::FloatRect(position.x,position.y,width,height); }
		virtual ~Object();
		virtual ObjectType GetObjectType() { return ObjectType::Object_Base; }
		virtual void onInit() = 0;
		virtual void onDeinit() = 0;
		virtual void onUpdate() = 0;
		virtual void onInput(const sf::Event &e) = 0;
		virtual void onDraw(sf::RenderTarget &target) = 0;
		virtual void onCollision(Object *other) = 0;
		virtual void onDestroy() = 0;
		inline bool IsAlive() { return alive; }
		float GetDistance(Object *other);
	};
}

