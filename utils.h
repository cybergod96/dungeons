#pragma once
#include <SFML/Graphics.hpp>
#include <direct.h>
#include <string>
#include <sstream>

std::string GetLoadingPath(std::string file_name);
void ShowText(sf::RenderTarget &target, sf::Vector2f pos, sf::String str, sf::Font &font, unsigned size);
void ShowText_Centered(sf::RenderTarget &target, sf::FloatRect center_box, sf::String str, sf::Font &font, unsigned size);
float GetDistance(sf::Vector2f pos1, sf::Vector2f pos2);